There may be times that you would like to prevent other standard users from shutting down or restarting the Windows 10/8/7 computer. This article will tell you how you can do so by creating a separate Group Policy object for non-administrators. When you enable this setting, the shutdown, restart, sleep, and hibernate buttons will be removed.

Prevent access to shutdown, restart, sleep, hibernate commands
To do so, type mmc in start search and hit Enter to open the Microsoft Management Console. In the File tab, click on Add/Remove Snap-in.
