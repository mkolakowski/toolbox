# toolbox
Disclaimer: This is a collection of software that I have curated for my own use. There is no warranty on anything in this collection, run at your own risk. If your software is in this repo and you do not want it hosted please let me know and I will remove it


For windows users follow the below steps to setup repo sync

1. Download and install Github Deskop https://desktop.github.com/
2. Skip the Sign into github (unless you would like to)
3. Choose "Clone a repository from the Internet..."
4. Choose URL and enter: https://gitlab.com/mkolakowski/toolbox.git
5. Github desktop will ask for a username and password, enter your Gitlab username and go to https://gitlab.com/-/profile/personal_access_tokens and generate an access token and enter the token in the password field.
6. Now the repo should start to sync down
