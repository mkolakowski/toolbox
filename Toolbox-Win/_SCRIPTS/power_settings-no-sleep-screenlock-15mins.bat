REM Batch File to Disable all Sleep Parameters

@echo off

powercfg /x -hibernate-timeout-ac 0
powercfg /x -hibernate-timeout-dc 0
powercfg /x -disk-timeout-ac 0
powercfg /x -disk-timeout-dc 0
powercfg /x -monitor-timeout-ac 15
powercfg /x -monitor-timeout-dc 15
Powercfg /x -standby-timeout-ac 0
powercfg /x -standby-timeout-dc 0

Pause
