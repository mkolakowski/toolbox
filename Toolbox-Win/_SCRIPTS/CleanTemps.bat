@echo off
if exist C:\Setup\LastCleanup.txt (
    rem file exists
) else (
    cleanmgr /sageset:99st
    
)
rem cleanmgr /sageset:99
mkdir c:\setup 
 cls
 del /q /s "C:\Users\%USERNAME%\AppData\Local\Temp\*.*" > C:\setup\LastCleanup.txt
 del /q /s "C:\Windows\Temp\*.*" >>C:\LastCleanup.txt
 del /q /s "C:\Temp\*.*" >>C:\LastCleanup.txt
 cls
 cls
@echo.
@echo.
@echo.
@echo.
@echo.
@echo.
@echo.
@echo.
@echo     The IT Department is cleaning the temporary files from this PC. 
@echo.	This process will help your machine run more efficiently and faster.
@echo.
@echo		                  Please be patient...
@echo.
@echo.
@echo.
@echo.
@echo.
@echo.
@echo.
@echo.

 cleanmgr /sagerun:99
 rem date /T >>c:\setup\Lastcleanup.txt
