<#
.SYNOPSIS
Downloads files needed to download and install the latest versions of office 365

.DESCRIPTION
Downloads a setup.exr provided by Microsoft and downloads the chosed XML files to download and install office 365 of a chosen build

.FUNCTIONS
     o365Folder-Path-Check
          Checks that the $o365folder exists on the client systems

     Install-Office
          Downloads then installs office 365

     Install-Office-ProPlus-64
          Downloads the office 365 XML and calls the office 365 install function



.NOTES
Created by Matthew Kolakowski on 7/6/2021
Complete testing on all operating systems has NOT been done on this script yet, so run this at your own risk and further research it.
#>
$date                    = (get-date).toString("yyyy-MM-dd hh-mm-ss tt")

$o365folder              = "C:\odt"
$XMLpath                 = "$o365folder\office.xml"
$URLsetupEXE             = "https://gitlab.com/mkolakowski/toolbox/-/raw/main/Toolbox-Win/Office%20365%20Deployment%20Tool/setup.exe"
$SetupEXE                = "$o365folder\setup.exe"
$XMLtemp                 = " "

$XMLOfficeProPlus64      = "https://gitlab.com/mkolakowski/toolbox/-/raw/main/Toolbox-Win/Office%20365%20Deployment%20Tool/installOfficeProPlus64.xml"
$XMLOfficeProPlus32      = "https://gitlab.com/mkolakowski/toolbox/-/raw/main/Toolbox-Win/Office%20365%20Deployment%20Tool/installOfficeProPlus32.xml"
$XMLOfficeBusRet64       = "https://gitlab.com/mkolakowski/toolbox/-/raw/main/Toolbox-Win/Office%20365%20Deployment%20Tool/installOfficeBusRet64.xml"
$XMLOfficeBusRet32       = "https://gitlab.com/mkolakowski/toolbox/-/raw/main/Toolbox-Win/Office%20365%20Deployment%20Tool/installOfficeBusRet32.xml"

function o365Folder-Path-Check
{
     if ( Test-Path -Path  $o365folder)
     {
          #do nothing folder exists
     }
          else
          {
               #Write-Host "Creating $o365folder"
               mkdir $o365folder
          }
}

function Download-Office-Setup
{
     #If the file does not exist, create it.
     if (-not(Test-Path -Path $SetupEXE -PathType Leaf)) 
     {
          #Write-Host "Downloading setup.exe"
          & Invoke-WebRequest -Uri $URLsetupEXE -OutFile $SetupEXE
     }
     # If the file already exists, show the message and do nothing.
     else 
     {
          Write-Host "Cannot create [$SetupEXE] because a file with that name already exists."
     }
}

function Download-Office-XML
{
     #If the file does not exist, create it.
     if (Test-Path -Path $XMLpath -PathType Leaf)
     {
          #do nothing folder exists
     }
     else
     {
          Write-Host "Downloading $XMLpath"
          & Invoke-WebRequest -Uri $XMLtemp -OutFile $XMLpath
     }

}


function Install-Office
{
     Download-Office-Setup
     Write-Host "Downloading Office 365 (This can take some time)"
     & $SetupEXE /download $XMLpath
     Write-Host "Installing Office 365 (This can take some time)"
     #& $SetupEXE /configure $XMLpath
}

function Install-Office-ProPlus-64
{
     o365Folder-Path-Check
     $XMLtemp = $XMLOfficeProPlus64
     Download-Office-XML
     Install-Office
}

function Install-Office-ProPlus-32
{

}

function Install-Office-Business-64
{

}

function Install-Office-Business-32
{

}


function Show-Menu
{
     param 
     (
           [string]$Title = 'Office 365 Installer'
     )
     cls
     Write-Host "================ $Title ================"
    
     Write-Host "1: Press '1' To install Pro Plus 64 Bit."
     Write-Host "2: Press '2' To install Pro Plus 32 Bit."
     Write-Host "3: Press '3' To install Business 64 Bit."
     Write-Host "3: Press '4' To install Business 32 Bit."
     Write-Host "Q: Press 'Q' to quit."
}

do
{
     Show-Menu
     $input = Read-Host "Please make a selection"
     switch ($input)
     {
           '1' {
                cls
                'You chose To install Pro Plus 64 Bit'
                Install-Office-ProPlus-64
           } '2' {
                cls
                'You chose To install Pro Plus 32 Bit'
                Install-Office-ProPlus-32

           } '3' {
                cls
                'You chose To install Business 64 Bit'
                Install-Office-Business-64

           } '4' {
                cls
                'You chose To install Business 32 Bit'
                Install-Office-Business-32

           } 'q' {
                return
           }
     }
     pause
}
until ($input -eq 'q')
