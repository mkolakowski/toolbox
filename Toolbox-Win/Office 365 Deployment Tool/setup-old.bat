@ECHO off
cls
:start
mkdir C:\odt
cd C:\odt
ECHO 
ECHO ------------------------------
ECHO 1. Pro Plus 64 bit - Install
ECHO 2. Pro Plus 32 bit - Install
ECHO ------------------------------
ECHO 3. Business 64 bit - Install
ECHO 4. Business 32 bit - Install
ECHO ------------------------------
ECHO 0. Exit
ECHO ------------------------------
set choice=
set /p choice=Type the number to print text.
if not '%choice%'=='' set choice=%choice:~0,1%
if '%choice%'=='1' goto installOfficeProPlus64download
if '%choice%'=='2' goto installOfficeProPlus32download
if '%choice%'=='3' goto installOfficeBusRet64download
if '%choice%'=='4' goto installOfficeBusRet32download
if '%choice%'=='0' goto end

ECHO "%choice%" is not valid, try again
ECHO.
goto start


:installOfficeProPlus64download
ECHO "Downloading Office 365 Pro Plus 64 bit"
ECHO "This will take some time"
curl.exe -o C:\odt\setup.exe https://gitlab.com/mkolakowski/toolbox/-/raw/main/Toolbox-Win/Office%20365%20Deployment%20Tool/setup.exe
curl.exe -o C:\odt\office.xml https://gitlab.com/mkolakowski/toolbox/-/raw/main/Toolbox-Win/Office%20365%20Deployment%20Tool/installOfficeProPlus64.xml
goto installOffice

:installOfficeProPlus32download
ECHO "Downloading Office 365 Pro Plus 32 bit"
ECHO "This will take some time"
curl.exe -o C:\odt\setup.exe https://gitlab.com/mkolakowski/toolbox/-/raw/main/Toolbox-Win/Office%20365%20Deployment%20Tool/setup.exe
curl.exe -o C:\odt\office.xml https://gitlab.com/mkolakowski/toolbox/-/raw/main/Toolbox-Win/Office%20365%20Deployment%20Tool/installOfficeProPlus32.xml
goto installOffice

:installOfficeBusRet64download
ECHO "Downloading Office 365 Business 64 bit"
ECHO "This will take some time"
curl.exe -o C:\odt\setup.exe https://gitlab.com/mkolakowski/toolbox/-/raw/main/Toolbox-Win/Office%20365%20Deployment%20Tool/setup.exe
curl.exe -o C:\odt\office.xml https://gitlab.com/mkolakowski/toolbox/-/raw/main/Toolbox-Win/Office%20365%20Deployment%20Tool/installOfficeBusRet64.xml
goto installOffice

:installOfficeBusRet32download
ECHO "Downloading Office 365 Business 32 bit"
ECHO "This will take some time"
curl.exe -o C:\odt\setup.exe https://gitlab.com/mkolakowski/toolbox/-/raw/main/Toolbox-Win/Office%20365%20Deployment%20Tool/setup.exe
curl.exe -o C:\odt\office.xml https://gitlab.com/mkolakowski/toolbox/-/raw/main/Toolbox-Win/Office%20365%20Deployment%20Tool/installOfficeBusRet32.xml
goto installOffice

:installOffice
C:\odt\setup.exe /download office.xml
C:\odt\setup.exe /configure office.xml
goto end



:end
ECHO GOODBYE
