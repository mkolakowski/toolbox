
**_INSTALLERS**

- 7z - 1900 - Installer - x64.exe

- Adobe - Acrobat Cleaner - 10.x-11.x.exe

- Adobe - Acrobat Cleaner - 9.x.exe

- Adobe - Acrobat Cleaner - DC2015.exe

- Adobe - Acrobat Reader DC - 1900820081.zip

- Adobe_Application_Manager_Set-Up.exe

- Adobe_readerdc_en_xa_crd_install.exe

- Advanced IP Scanner - 2.5.3850.exe

- Albus Bit - NTFS Permissions Auditor - Installer - 1.3.0.0.exe

- Belarc - Advisor - Installer.exe

- CutePDF - Writer - Installer - v4.0.exe

- ELO - Touch Screen Driver - 5.5.1.exe

- Evolis - Badge Creator Premium Suite - 6.32.3.1481 - Install.exe

- F.lux - Installer.exe

- Firefox - Online Installer.exe

- FortiClient - VPNSetup - Offline Installer - 6.2.0 - x64.exe

- FortiClient - VPNSetup - Offline Installer - 6.4.0 - x64.exe

- FreeFileSync - 11.3 - Installer.exe

- Google - Chrome - Offline Installer - x64.msi

- Google - Chrome - Online Installer - x64.exe

- Greenshot - Installer - 1.2.10.6.exe

- MalwareBytes - Installer - 4.1.0.159-1.0.920-1.0.24046.exe

- MalwareBytes - Installer - 4.2.1.179-1.0.1045-1.0.29939.exe

- Microsoft - Access Runtime - 2013 x86.exe

- Microsoft - Access Runtime 2016 x86 - Installer.exe

- Microsoft - dotNet 3.5 - Offline setup.exe

- Microsoft - dotNet 3.5 - Online setup.exe

- Microsoft - IdFix tool.exe

- Microsoft - MDAC (Microsoft Data Access Components) - 2.8 SP1.EXE

- Microsoft - Media Creation Tool - 1909.exe

- Microsoft - Media Creation Tool - 20H2.exe

- Microsoft - Office Scruber - Portable.exe

- Microsoft - OneNote 2016 - Installer.exe

- Microsoft - RMSC (Rights Management Service Client) - Installer - 2.1 - x64.exe

- Microsoft - Surface Diagnostic Toolkit for Business - v2.131.139.0.msi

- Microsoft - Surface Diagnostic Toolkit SA.exe

- Microsoft - Sysinternals - Autoruns.exe

- Microsoft - Teams - Installer.exe

- Microsoft - Visual C++ 2005 SP1 - x64.exe

- Microsoft - Visual C++ 2005.exe

- Microsoft - Visual C++ 2008 - x86.exe

- MouseJiggle - Portable.exe

- Ninite - 7Zip Chrome - Installer.exe

- Ninite - 7Zip Chrome Firefox VLC - Installer.exe

- Nirsoft - BlueScreenView - Portable.exe

- Nirsoft - BrowsingHistoryView - Portable.exe

- Nirsoft - PingInfoView - Portable.exe

- Opera - Portable.exe

- Prinform - Recuva - Installer -v153.exe

- Printers (Network Printer Scan) - Portable.exe

- Putty - Installer - 0.74.msi

- Python - Installer - 3.8.5.exe

- shadowcopysetup.exe

- Sidder - User profile Disk Viewer.exe

- VLC - Installer - 3.0.12.exe

- VMware - Enhanced Authentication Plugin - Installer - 6.5.0.exe

- VMware - Enhanced Authentication Plugin - Installer - 6.7.0.exe

- VMware - VMRC - Installer - 10.0.4-11818843.exe

- WinSCP-5.15.9-Setup.exe

- WinSCP-5.17.7-Setup.exe

- wiztree_3_32_setup.exe

- wiztree_3_35_portable_x64.exe

- wiztree_3_35_portable_x86.exe

- wiztree_3_35_setup.exe
